#!/usr/bin/python

import os
import json
from experiment import Experiment
#from simscaffold import ExampleSimulation
from simscaffold import ExamplePythonSimulation

class Experiment1(Experiment):
    def __init__(self):
        d,f = os.path.split(__file__)
        Experiment.__init__(self, 1, d)
        self.description = 'ExamplePythonSimulation Test Experiment'

    def run_the_experiment(self):
        s = ExamplePythonSimulation()
        for i in range(1,10):
            measurement_id = self.new_measurement()
            s.N = i
            s.measurement_id = measurement_id
            s.experiment_dir = self.experiment_dir
            s.init()
            s.run()
            a = s.average()
            print(a)
            self.save_measurement(measurement_id, s)

if __name__ == '__main__':
    e = Experiment1()
    e.run()
