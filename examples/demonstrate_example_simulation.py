from simscaffold import ExampleSimulation

if __name__ == '__main__':
    s = ExampleSimulation()

    # set parameters
    s.N_std = 4
    s.N_row = 4
    s.N_col = 8

    # init and run
    s.init()
    s.run()

    # retrieve results
    a = s.average()
    print('Results: {}'.format(a))

    # demonstrate C++/Eigen-Numpy integration
    print('\n\nPrinting from C++:\n')
    s.print_all()

    print('\n\nPrinting from Python:\n')
    print('stdvector')
    print(s.stdvector_as_numpy_array())
    print('eigenvector')
    print(s.eigenvector_as_numpy_array())
    print('eigenmatrix')
    print(s.eigenmatrix_as_numpy_array())

    print('\n\nChanging the matrix in Python changes the matrix in C++ --- they are the same.')
    a = s.eigenmatrix_as_numpy_array()
    a[2,2] = 100
    a[3,3] = 200
    print('\nPython')
    print(a)
    print('\nC++')
    s.print_all()

