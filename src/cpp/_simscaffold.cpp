#include <string>
#include <iostream>
#include <cstdlib>

#include <boost/python.hpp>
#include <boost/numpy.hpp>

#include <Eigen/Dense>

#include "version.hpp"

namespace p = boost::python;
namespace np = boost::numpy;

// TODO: separate pure C++ and the Python module wrapper, i.e. have a pure C++
// simulation class and add Python support non-instrusively, if possible

/**
 * An example numerical simulation class.
 *
 * This class demonstrates two things: The general workflow for a simulation and
 * some examples of interfacing between Python and C++.
 *
 * General workflow
 * ----------------
 *
 *  Create an instance of the simulation class.
 *  Set the parameters, e.g. N_std, N_eigen.
 *  init
 *  run
 *  Retrieve the results.
 *
 * Python-C++ interface
 * --------------------
 *
 *  This class is intended to be compiled as a Python module. The
 *  `stdvector_...` and `eigen...` methods demonstrate how to return the C++
 *  std::vector and Eigen vectors and matrices as Python Numpy arrays.
 */
class ExampleSimulation
{
public:
    // set from outside to configure the simulation
    // size of the stdvector, eigenvector and eigenmatrix
    size_t N_std, N_eigen, N_row, N_col;

    // read-only to the outside
    const std::string program = "ExampleSimulation";
    const std::string version = PROGRAM_VERSION;
    std::vector<double> stdvector;
    Eigen::VectorXd eigenvector;
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> eigenmatrix;
    double average_stdvector, average_eigenvector, average_eigenmatrix;
    bool got_result = false;
    
    /*
     * Note: Eigen defaults to column-major matrices while Numpy defaults to
     * row-major matrices. Both can be configured to use either memory layout
     * scheme.
     *
     * It probably is a good idea to use the same memory layout scheme
     * throughout our program, hence here we use a row-major Eigen matrix
     * (defined above).
     *
     * The following two lines are equivalent. Both define a column-major Eigen
     * matrix:
     * Eigen::MatrixXd eigenmatrix;
     * Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> eigenmatrix;
     */

public:
    ExampleSimulation()
    : N_std(5), N_eigen(10), N_row(3), N_col(5)
    {}

    /**
     * Initialize the simulation.
     */
    void init()
    {
        got_result = false;
        stdvector.resize(N_std,0);
        eigenvector.resize(N_eigen);
        eigenmatrix.resize(N_row, N_col);
    }

    /**
     * Run the simulation.
     *
     * Here we just fill the vectors and the matrix, as an example.
     */
    void run()
    {
        for (size_t i=0; i<N_std; i++)
            stdvector[i] = i+1;
        for (size_t i=0; i<N_eigen; i++)
            eigenvector(i) = i+1;
        for (size_t j=0; j<N_col; j++)
            for (size_t i=0; i<N_row; i++)
                eigenmatrix(i,j) = i*N_col+j+1;
    }

    p::tuple average()
    {
        average_stdvector = 0;
        for (double d : stdvector)
            average_stdvector += d;
        average_stdvector /= stdvector.size();
        average_eigenvector = eigenvector.sum() / eigenvector.size();
        average_eigenmatrix = eigenmatrix.sum() / eigenmatrix.size();
        
        got_result = true;
        return p::make_tuple(average_stdvector, average_eigenvector, average_eigenmatrix);
    }

    /**
     * Retrieve the C++ std::vector as a Python list.
     *
     * Copies the data.
     */
    p::list stdvector_as_list()
    {
        // TODO: I am not sure why this does not work
        // p::object get_iterator = p::iterator<std::vector<double> >();
        // p::object i = get_iterator(stdvector);
        // p::list l(i);
        
        p::list l;
        for (double d : stdvector)
        {
            l.append(d);
        }
        return l;
    }

    /**
     * Retrieve the C++ std::vector as a Numpy array (copy).
     *
     * Copies the data.
     */
    np::ndarray stdvector_as_numpy_array_copy()
    {
        //p::Py_intptr_t shape[1] = { v.size() };
        p::tuple shape = p::make_tuple(stdvector.size());
        np::dtype dtype = np::dtype::get_builtin<double>();
        np::ndarray a = np::zeros(shape, dtype);
        std::copy(stdvector.begin(), stdvector.end(), reinterpret_cast<double*>(a.get_data()));
        return a;
    }

    /**
     * Retrieve the C++ std::vector as a Numpy array.
     */
    np::ndarray stdvector_as_numpy_array()
    {
        /*
         * std::vector::data() is C++11. Strides is the number of bytes to the
         * next element in the raw data array, for each dimension, e.g. for a
         * matrix it would be number of bytes to the next row, number of bytes
         * to the next column.
         */
        double* data_pointer = stdvector.data();
        np::dtype dtype = np::dtype::get_builtin<double>();
        p::tuple shape = p::make_tuple(stdvector.size());
        p::tuple strides = p::make_tuple(sizeof(double));
        np::ndarray a = np::from_data(data_pointer, dtype, shape, strides, p::object());
        return a;
    }

    /**
     * Retrieve the Eigen vector as a Numpy array.
     */
    np::ndarray eigenvector_as_numpy_array()
    {
        double* data_pointer = eigenvector.data();
        np::dtype dtype = np::dtype::get_builtin<double>();
        p::tuple shape = p::make_tuple(eigenvector.size());
        p::tuple strides = p::make_tuple(sizeof(double));
        np::ndarray a = np::from_data(data_pointer, dtype, shape, strides, p::object());
        return a;
    }

    /**
     * Retrieve the Eigen matrix as a Numpy array.
     */
    np::ndarray eigenmatrix_as_numpy_array()
    {
        double* data_pointer = eigenmatrix.data();
        np::dtype dtype = np::dtype::get_builtin<double>();
        p::tuple shape = p::make_tuple(eigenmatrix.rows(), eigenmatrix.cols());
        p::tuple strides = p::make_tuple(eigenmatrix.cols() * sizeof(double), sizeof(double));
        /*
         * For column-major matrices (Eigen default), we would have:
         * p::tuple strides = p::make_tuple(sizeof(double), eigenmatrix.rows() * sizeof(double));
         */
        np::ndarray a = np::from_data(data_pointer, dtype, shape, strides, p::object());
        return a;
    }

    /**
     * Create and return a 3x3 zero Numpy array.
     */
    np::ndarray zero_numpy_array()
    {
        p::tuple shape = p::make_tuple(3,3);
        np::dtype dtype = np::dtype::get_builtin<double>();
        np::ndarray a = np::zeros(shape, dtype);
        return a;
    }

    /**
     * Print the C++ std::vector and the Eigen vector and matrix to stdout.
     *
     * For debugging and testing.
     */
    void print_all()
    {
        std::cout << "stdvector" << std::endl;
        for (double d : stdvector)
        {
            std::cout << d << std::endl;
        }
        std::cout << std::endl;
        std::cout << "eigenvector" << std::endl;
        std::cout << eigenvector << std::endl << std::endl;
        std::cout << "eigenmatrix" << std::endl;
        std::cout << eigenmatrix<< std::endl << std::endl;
    }
};

// Declare the Python module
BOOST_PYTHON_MODULE (_simscaffold)
{
    // This will enable user-defined docstrings and python signatures,
    // while disabling the C++ signatures
    p::docstring_options my_docstring_options(true, true, false);

    np::initialize();
    p::class_<ExampleSimulation>("ExampleSimulation",
                                 "An example numerical simulation class.")
        .def("init",
             &ExampleSimulation::init,
             "Initialize the simulation.")
        .def("run",
             &ExampleSimulation::run,
             "Run the simulation.")
        .def("average",
             &ExampleSimulation::average,
             "Calculate the average of the C++ std::vector and the Eigen vector and matrix.")
        .def("stdvector_as_list",
             &ExampleSimulation::stdvector_as_list)
        .def("stdvector_as_numpy_array_copy",
             &ExampleSimulation::stdvector_as_numpy_array_copy)
        .def("stdvector_as_numpy_array",
             &ExampleSimulation::stdvector_as_numpy_array)
        .def("eigenvector_as_numpy_array",
             &ExampleSimulation::eigenvector_as_numpy_array)
        .def("eigenmatrix_as_numpy_array",
             &ExampleSimulation::eigenmatrix_as_numpy_array)
        .def("zero_numpy_array",
             &ExampleSimulation::zero_numpy_array)
        .def("print_all",
             &ExampleSimulation::print_all, 
             "Print the C++ std::vector and the Eigen vector and matrix to stdout.")
        .def_readonly("program",
                      &ExampleSimulation::program)
        .def_readonly("version",
                      &ExampleSimulation::version)
        .def_readwrite("N_std",
                       &ExampleSimulation::N_std,
                       "Size of the C++ std::vector.")
        .def_readwrite("N_eigen",
                       &ExampleSimulation::N_eigen,
                       "Size of the Eigen vector.")
        .def_readwrite("N_row",
                       &ExampleSimulation::N_row,
                       "Number of rows of the Eigen matrix.")
        .def_readwrite("N_col",
                       &ExampleSimulation::N_col,
                       "Number of columns of the Eigen matrix.")
    ;
}
