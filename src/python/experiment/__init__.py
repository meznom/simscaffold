__version__ = 'unknown'

try:
    from ._version import __version__
except ImportError:
    pass

from .directory import Directory
from .experiment import Experiment
from .commandlineinterface import CommandLineInterface
