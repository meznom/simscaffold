import os
import json

class DirectoryError (Exception):
    pass

class Directory (object):
    def __init__ (self, path = '~/experiments'):
        self.path = os.path.expanduser(path)
        self.indexfile = self.path + '/experiments.index'
        if not os.path.exists(self.path):
            os.mkdir(self.path)
            self.create_empty_indexfile()
        elif not os.path.exists(self.indexfile):
            raise ExperimentsDirectoryError('Not an experiments directory: "{}" '
                    'does not contain an index file "experiments.index"'.format(self.path))

    def create_empty_indexfile (self):
        o = {'lastId': 0}
        if os.path.exists(self.indexfile):
            raise ExperimentsDirectoryError('Can\'t create index file: File already exists')
        f = open(self.indexfile, 'w')
        json.dump(o,f)
        f.close()

    def last_id (self):
        f = open(self.indexfile)
        o = json.load(f)
        f.close()
        return int(o['lastId'])

    def path_of_experiment (self, experiment_id):
        return '{}/{:06d}'.format(self.path, experiment_id)


    def new_experiment (self):
        lockfile = self.indexfile + '.lock'
        if os.path.exists(lockfile):
            raise ExperimentsDirectoryError('Index file is locked')
        open(lockfile,'a').close()
        
        f = open(self.indexfile, 'r+')
        o = json.load(f)
        o['lastId'] += 1
        last_id = o['lastId']
        
        f.truncate(0)
        f.seek(0)
        json.dump(o,f)
        f.close()
        
        os.remove(lockfile)

        p = self.path_of_experiment(last_id)
        os.mkdir(p)
        return (last_id, p)
