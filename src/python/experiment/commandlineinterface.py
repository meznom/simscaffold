import os
import sys
import stat
from string import Template
import argparse
from .directory import Directory
from . import __version__

class CommandLineInterface(object):
    def __init__(self):
        self.directory = None

    def _get_parser(self):
        parser = argparse.ArgumentParser(
                description='Manage computational "experiments".')
        parser.add_argument(
                'action', 
                help='Create a new experiment', 
                choices=['create'])
        parser.add_argument(
                '-d', '--directory', 
                help='directory where all experiments are stored; default: ~/experiments', 
                default='~/experiments')
        parser.add_argument(
                '--version', 
                action='version', 
                version=__version__)
        return parser

    def create_experiment(self):
        print('Creating experiment')
        ed = Directory(self.directory)
        i,p = ed.new_experiment()
        print('Created experiment {} at {}'.format(i,p))

        # Create runexperiment.py script from template
        dst = 'runexperiment.py'
        src = 'runexperiment.py.template'
        d,f = os.path.split(__file__)
        src_file = os.path.join(d,src)
        dst_file = os.path.join(p,dst)
        
        f = open(src_file)
        s = f.read()
        f.close()
        t = Template(s)
        s = t.substitute(experimentid=i,python=sys.executable)
        f = open(dst_file, 'w')
        f.write(s)
        f.close()
        os.chmod(os.path.join(p,dst_file), 
                 stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | 
                 stat.S_IRGRP | stat.S_IXGRP | 
                 stat.S_IROTH | stat.S_IXOTH)
        print('Created script {}'.format(dst_file))
        print('')

    def main(self, argv):
        parser = self._get_parser()
        args = parser.parse_args()
        self.directory = args.directory
        if args.action == 'create':
            self.create_experiment()
