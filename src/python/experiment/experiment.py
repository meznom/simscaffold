from collections import OrderedDict
import datetime
import os
import json

class Experiment (object):
    def __init__(self, experiment_id=None, experiment_dir=None):
        self.experiment_dir=os.getcwd()
        if experiment_dir:
            self.experiment_dir = experiment_dir
        self.infofile = os.path.join(self.experiment_dir, 'experiment.info.json')
        self.experiment_id = experiment_id
        self.description = None
        self.start_date = None
        self.end_date = None
        self.state = None
        self.measurements = []

    def __getstate__(self):
        i = OrderedDict([
            ('info', OrderedDict([
                        ('experiment_id', self.experiment_id),
                        ('description', self.description),
                        ('start_date', self.start_date),
                        ('end_date', self.end_date),
                        ('state', self.state)])),
            ('measurements', self.measurements)
            ])
        return i

    # TODO: probably rename to 'save'
    def save_info(self):
        backupfile = self.infofile + '.backup'
        if os.path.exists(self.infofile):
            os.rename(self.infofile, backupfile)
        f = open(self.infofile, 'w')
        json.dump(self, f, default=Experiment._toJsonHook, indent=2, separators=(',',': '))
        f.close()
        if os.path.exists(backupfile):
            os.remove(backupfile)

    def load(self):
        # TODO: reconstruct class from info file
        pass

    def start(self):
        self.start_date = self.date_string()
        self.state = 'running'

    def end (self):
        self.end_date = self.date_string()
        self.state = 'done'

    def date_string(self):
        return datetime.datetime.utcnow().replace(microsecond=0).isoformat() + 'Z'

    def run(self):
        self.start()
        self.save_info()
        self.run_the_experiment()
        self.end()
        self.save_info()

    def run_the_experiment(self):
        raise NotImplementedError()

    def new_measurement(self):
        self.measurements.append(OrderedDict([('info', OrderedDict())]))
        return len(self.measurements)

    def start_measurement(self, measurement_id):
        m = self.measurements[measurement_id-1]
        m['info']['start_date'] = self.date_string()

    def end_measurement(self, measurement_id):
        m = self.measurements[measurement_id-1]
        m['info']['end_date'] = self.date_string()

    def save_measurement(self, measurement_id, m):
        i = OrderedDict()
        if hasattr(m, 'program'):
            i['program'] = m.program
        if hasattr(m, 'version'):
            i['version'] = m.version
        i['measurement_id'] = measurement_id
        i['measurement_dir'] = self.experiment_dir
        i['start_date'] = self.measurements[measurement_id-1]['info']['start_date']
        i['end_date'] = self.measurements[measurement_id-1]['info']['end_date']
        
        j = json.loads(
                json.dumps(m, default=Experiment._toJsonHook, indent=None, separators=(',',':')),
                object_pairs_hook=Experiment._fromJsonHook)
        
        if not j.has_key('info'):
            j = OrderedDict([('info', i)] + j.items())
        else:
            i.update(j['info'])
            j['info'] = i
        self.measurements[measurement_id-1] = j

    def save_results(self, measurement_id, r):
        m = self.measurements[measurement_id-1]
        if not m.has_key('results'):
            m['results'] = r
        else:
            m['results'].update(r)

    def get_measurements(self):
        return self.measurements

    @staticmethod
    def _toJsonHook (o):
        if hasattr(o, '__getstate__'):
            d = OrderedDict()
            d['type'] = str(o.__class__)
            d.update(o.__getstate__())
            return d
        raise TypeError(repr(o) + ' is not JSON serializeable')

    @staticmethod
    def _fromJsonHook(ps):
        return OrderedDict(ps)
