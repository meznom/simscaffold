# SimScaffold

Establish conventions and provide a framework for data management for
computational condensed matter projects.

Right now this repository holds two different parts.

1. Experiment (Python module `experiment`)
2. Example Simulations (in Python module `simscaffold`)

At this stage this really only is some code to test out ideas. I am not sure
whether the concepts and ideas presented below will work out at all. Therefore,
I would truly appreciate your thoughts and feedback.

## Concepts

### Simulation

A simulation class holds a numerical simulation. It can be configured (setting
input parameters), initialized and run, typically in that order. Afterwards
results are retrieved. A simulation should have a (program) name and a version.
For example:

    s = ExampleSimulation()

    # set parameters
    s.N_std = 4
    s.N_row = 4
    s.N_col = 8

    # init and run
    s.init()
    s.run()

    # retrieve results
    a = s.average()
    print(s.program)
    print(s.version)
    print('Results: {}'.format(a))

### Measurement

A measurement is one run of one specific simulation with one set of input
parameters and hence a unique computational result. In practice a measurement is
likely very similar to the simulation class itself, but maybe with additional
meta data, like start date, end date, and measurement id.

### Experiment

An experiment is a set of related measurements. It is therefore simply a means
of grouping measurements. Examples could be a parameter scan for some
simulation, or running a chain of simulations, where simulation 2 processes the
results of simulation 1 and so on. All data related to the experiment are kept
together (typically in one directory). In addition to raw data files from the
simulation, it contains a script to run the experiment, postprocess data,
create graphs, and possibly other tasks. Most importantly it contains a file in
a standard format, with a conventional data layout and a common filename that
describes the experiment: Input data, results, meta data. By following
conventions for the data and file layout, it should be easier to establish best
practices and common data management tools that work similar for very different
projects.

## Experiment Python Module

TODO

## Example Simulations

TODO

* Python/C++
* Serialization

1. Pure Python
2. Python/C++
3. Pure C++

## Building and Installation

Requirements:

* cmake
* python
* boost
* Boost.Numpy <https://github.com/ndarray/Boost.NumPy>

To build and install Boost.Numpy:

    $ git clone git://github.com/ndarray/Boost.NumPy.git
    $ cd Boost.NumPy/
    # install scons, if necessary
    $ sudo scons --with-boost=/opt/local/ install

Looks like newer versions of Boost.Numpy also have a cmake build system.

Build and install the simscaffold Python Module into the home directory:

    $ mkdir build
    $ cd build
    $ CXX=/opt/local/bin/g++-mp-4.7 cmake ..
    $ make
    $ cd ..
    $ python setup.py install --user
